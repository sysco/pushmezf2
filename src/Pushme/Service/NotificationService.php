<?php

namespace Sysco\Pushme\Service;

use \Exception;
use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;

/**
 * Main Service for communicating with Pushme
 * @author José Carlos Chávez <jose.carlos.chavez@sysco.no>
 */
class NotificationService
{

    /**
     * The apikey for the application.
     * @var striong
     */
    protected $apiKey;

    /**
     * The uri of the API service.
     * @var string 
     */
    CONST API_URI = 'http://dev.pushme.no/api';

    /**
     * 
     * @param string $apikey
     */
    public function __construct($apikey)
    {
        $this->apiKey = $apikey;
    }

    /**
     * Register a device in Pushme
     * @param type $params
     * @return array
     */
    public function registerDevice(array $params)
    {
        return $this->callApi('device/register', $params);
    }

    /**
     * Deregister a device in pushme by the deviceUid
     * @param string $uid
     * @return array
     */
    public function deregisterDevice($uid)
    {
        return $this->callApi('device/deregister', array(
                    'deviceUid' => $uid
        ));
    }

    public function sendMessage($params)
    {
        return $this->callApi('message/create', $params);
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * 
     * @param array $params
     * @return type
     */
    private function callApi($method, array $params)
    {
        $jsonParams = json_encode(array('params' => $params));

        try {
            $client = new Client(self::API_URI . '/' . $method);

            $client->setMethod('POST');
            $client->setRawBody($jsonParams);
            $client->setHeaders(array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonParams),
                'Authorization: ' . $this->getApiKey()
            ));

            $adapter = new Curl();

            $adapter->setCurlOption(CURLOPT_ENCODING, 'UTF-8');
            $adapter->setCurlOption(CURLOPT_RETURNTRANSFER, true);
            $adapter->setCurlOption(CURLOPT_CONNECTTIMEOUT , 0);
            $adapter->setCurlOption(CURLOPT_TIMEOUT, 360);

            $client->setAdapter($adapter);

            $response = $client->send();

            $rawResponseBody = $response->getBody();

            return Json::decode($rawResponseBody, Json::TYPE_ARRAY);
        } catch (Exception $e) {
            throw $e;
        }
    }

}
