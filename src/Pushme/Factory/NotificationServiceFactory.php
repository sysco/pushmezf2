<?php

namespace Sysco\Pushme\Factory;

use Zend\ServiceManager\FactoryInterface;
use Sysco\Pushme\Service\NotificationService;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Factory for NotificationService
 * @author José Carlos Chávez <jose.carlos.chavez@sysco.no>
 */
class NotificationServiceFactory implements FactoryInterface
{

    /**
     * 
     * @param ServiceLocatorInterface $serviceLocator
     * @return NotificationService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');

        $pushmeConfig = $config['pushme'];

        $notificationService = new NotificationService($pushmeConfig['apikey']);

        return $notificationService;
    }

}
